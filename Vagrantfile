
Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/xenial64"

  config.vm.network "forwarded_port", guest: 80, host: 8088, auto_correct: true
  config.vm.network "forwarded_port", guest: 443, host: 4445

  config.vm.network "public_network", bridge: "en6: AX88179 USB 3.0 to Gigabit Ethernet"

  config.vm.synced_folder "./", "/var/www/html"

  config.vm.provision "shell", inline: <<-SHELL
    sudo su

    apt-get install software-properties-common
    add-apt-repository ppa:ondrej/php
    add-apt-repository ppa:ondrej/apache2

    apt-get update

    apt-get install apache2 -y
    apt-get install memcached -y
    apt-get install php7.2 php7.2-mbstring php7.2-zip php7.2-xml php7.2-curl php7.2-gd php7.2-mysql libapache2-mod-php7.1 php-memcached -y

    a2enmod rewrite
    a2enmod ssl

    apt-get install composer -y

    apt install git -y
    apt-get install imagemagick -y
    apt-get install php-imagick -y

    rm /etc/apache2/sites-available/000-default.conf

    cat <<EOF >> /etc/apache2/sites-available/000-default.conf
    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        <Directory /var/www/html/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
        </Directory>
    </VirtualHost>
EOF

    service apache2 restart

    apt-get update
    apt-get upgrade -y
    apt-get autoremove -y
  SHELL
end
